
properties { 
  $solutionRoot = resolve-path .\..\src
  $solutionFile = "$solutionRoot\ClickOnceAndOctopusDeploy.sln"
  $clickOnceProjectDir = "$solutionRoot\WpfApplication1" 
  $clickOnceApplicationName = "WpfApplication1"
  $ClickOnceProjectFile = "$clickOnceApplicationName.nuspec"
  $nugetExe = ".\tools\NuGet\NuGet.exe"
  $roboCopyExe =".\tools\robocopy\robocopy.exe"
  $octoExe = ".\tools\octo\octo.exe"
  $buildConfiguration="release"
  $buildPlatform = "Any CPU"
  $OctopusApiKey = "000000000000000000000000000"
  $OctopusUrl = "http://octopus.com/api"
  $NugetApiKey = "00000000-0000-0000-0000-000000000000"
  $NugetFeedUrl = "http://nuget.feed.com"
 
}

task default -depends clean, build, package, publish, createRelease

task build {
	Write-Host "Build your solution..."
	Exec { msbuild "/p:configuration=$buildConfiguration"  /p:Platform=$buildPlatform /p:RunOctoPack=true  /t:Rebuild $solutionFile} "Failed to build $solutionFile"
}

task package -description "Copy application files to artifact directory and package as nupkg" {
	$clickOnceAppArtifactFolderName = 'ClickOnceArtifacts'
	$clickOnceAppOutputDir = "$clickOnceProjectDir\bin\$buildConfiguration"
	$clickOnceAppArtifactDir = "$clickOnceAppOutputDir\$clickOnceAppArtifactFolderName"
	$version = Get-AssemblyVersion "$clickOnceAppOutputDir\WpfApplication1.exe"
	$excludes = "$clickOnceAppOutputDir\deployment $clickOnceAppOutputDir\ClickOnceArtifacts"
	& $roboCopyExe $clickOnceAppOutputDir $clickOnceAppArtifactDir /xd $excludes /MOVE
	Exec { & $nugetExe pack "$clickOnceProjectDir\$ClickOnceProjectFile" -BasePath $clickOnceAppOutputDir -OutputDirectory $clickOnceAppOutputDir -version $version -Prop Configuration=Release -NoPackageAnalysis } "Failed to package clickonce application from artifact directory"
}

task clean -description "Cleanup any output directories"{
	if ((Test-Path -path "$clickOnceProjectDir\bin\$buildConfiguration"))
	{
		 Get-ChildItem $solutionRoot -include obj,publish,bin -Recurse | foreach ($_) {		
      		"Cleaning: " + $_.fullname
			remove-item $_.fullname -Force -Recurse 
	   	}
	}
}

task publish -description "Push all nupkg to nuget feed" {	
	#Write-Host "This needs configuration for your setup..."
	Get-ChildItem $solutionRoot -include "WpfApplication1*.nupkg","WebApplication1*.nupkg" -Recurse | foreach ($_) { 
		"Pushing " + $_.fullname
		 Exec {& $nugetExe push $_.fullname $NugetApiKey -source $NugetFeedUrl} "Failed to push $_.fullname to $NugetFeedUrl"
    }
	if ($lastExitCode -ne 0) {
        throw "Error: Failed to push nuget packages"
    }
}

task createRelease -description "Create a Octopus release and deploy to staging" {
	Write-Host "This needs configuration for your setup..."
	Exec { & $octoExe create-release --server=$OctopusUrl --project=ClickOnceAndOctopusDeploy --deployto=Dev  --apiKey=$OctopusApiKey --waitfordeployment=True } "Failed to create an octopus release"
	if ($lastExitCode -ne 0) {
        throw "Error: Failed to create and deploy octopus release"
    }
}

function Get-AssemblyVersion{
	param($file)
	return [System.Diagnostics.FileVersionInfo]::GetVersionInfo($file).FileVersion
}
